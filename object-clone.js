/////////////////////////////////////////////////////////////////////////////////////////////
//
// object-clone
//
//    Library for cloning javascript objects.
//
// License
//    Apache License Version 2.0
//
// Copyright Nick Verlinden (info@createconform.com)
//
///////////////////////////////////////////////////////////////////////////////////////////// 
/////////////////////////////////////////////////////////////////////////////////////////////
//
// Privates
//
/////////////////////////////////////////////////////////////////////////////////////////////
var Error = require("error");
var type =  require("type");

var ERROR_INVALID_OBJECT = "Invalid Object";

/////////////////////////////////////////////////////////////////////////////////////////////
//
// Functions
//
/////////////////////////////////////////////////////////////////////////////////////////////
function clone(obj) {
    /**
     * Deep copy an object (make copies of all its object properties, sub-properties, etc.)
     * An improved version of http://keithdevens.com/weblog/archive/2007/Jun/07/javascript.clone
     * that doesn't break if the constructor has required parameters
     *
     * It also borrows some code from http://stackoverflow.com/a/11621004/560114
     */

    // If Object.create isn't already defined, we just do the simple shim, without the second argument,
    // since that's all we need here
    var object_create = Object.create;
    if (typeof object_create !== "function") {
        object_create = function(o) {
            function F() {}
            F.prototype = o;
            return new F();
        };
    }

    if (type.isPrimitive(obj)) {
        return obj;
    }
    if (type.isArray(obj)) {
        var arr = [];
        for (var i in obj) {
            arr[i] = clone(obj[i]);
        }
        return arr;
    }
    if(obj == null) {
        return null;
    }
    if (!type.isObject(obj)) {
        throw new Error(ERROR_INVALID_OBJECT, "An instance of type '" + (typeof obj) + "' was given.");
    }

    // Initialize the visited objects array if needed
    // This is used to detect cyclic references
    if (typeof _visited === "undefined"){
        _visited = [];
    }

    // Otherwise, ensure obj has not already been visited
    else {
        var i, len = _visited.length;
        for (i = 0; i < len; i++) {
            // If obj was already visited, don't try to copy it, just return the reference
            if (obj === _visited[i]) {
                return obj;
            }
        }
    }

    // Add this object to the visited array
    _visited.push(obj);

    // Honor native/custom clone methods
    if(type.isFunction(obj.clone)){
        return obj.clone(true);
    }

    //Special cases:
    //Array
    if (Object.prototype.toString.call(obj) == "[object Array]") {
        //[].slice(0) would soft clone
        ret = obj.slice();
        var i = ret.length;
        while (i--){
            ret[i] = clone(ret[i], _visited);
        }
        return ret;
    }
    //Date
    if (obj instanceof Date){
        return new Date(obj.getTime());
    }
    //RegExp
    if(obj instanceof RegExp){
        return new RegExp(obj);
    }
    //DOM Elements
    if(obj.nodeType && type.isFunction(obj.cloneNode)){
        return obj.cloneNode(true);
    }

    //If we've reached here, we have a regular object, array, or function

    //make sure the returned object has the same prototype as the original
    var proto = (Object.getPrototypeOf ? Object.getPrototypeOf(obj): obj.__proto__);
    if (!proto) {
        proto = obj.constructor.prototype; //this line would probably only be reached by very old browsers
    }
    var ret = object_create(proto);

    for(var key in obj){
        //Note: this does NOT preserve ES5 property attributes like 'writable', 'enumerable', etc.
        //For an example of how this could be modified to do so, see the singleMixin() function
        ret[key] = clone(obj[key], _visited);
    }
    return ret;
}

/////////////////////////////////////////////////////////////////////////////////////////////
module.exports = clone;
module.exports.ERROR_INVALID_OBJECT = ERROR_INVALID_OBJECT;